#############################################################
# Project: Claxton Mammogram Screening
# Date: 9/12/2014
# Details: Pull all women within X distance 
#          until we get 28,170 + 10% Ctrl
#############################################################

load("cmty_area.RData")
temp<-cmty_area[cmty_area$TenantID==127,]
temp$ExactAge<-ifelse(temp$ExactAge=="90+",90,as.numeric(temp$ExactAge))

temp3<-temp[(temp$Gender=="Female") & !is.na(temp$Gender) & !(substr(temp$zipcode,1,1) %in% LETTERS),]
length(unique(temp3$AddressID[temp3$mailabilityscorebyte1==1]))

hosploc<-read.csv("hosploc.csv")
names(hosploc)[2]<-"facility"
hosploc1<-cbind(hosploc$Longitude[2],hosploc$Latitude[2])


zips<-get_zips(127)

zips1<-NULL
for (i in 1:length(zips[[1]])){
  if (i!=length(zips[[1]])) zips1<-paste0(zips1,"\'",zips[i,1],"\',") else
    zips1<-paste0(zips1,"\'",zips[i,1],"\'")
}
library(RODBC)
sql1<-odbcConnect("TncmSql1")
system.time(
  addr<-as.data.frame(sqlQuery(sql1,
                               paste0("SELECT 
                             familyid,
                             longitude,
                             latitude
                             FROM [NationalMarket].[dbo].[NationalHousehold]
WHERE  (zipcode IN (", zips1 ,"))")
                               ,errors=T,stringsAsFactors = F))
)


pll<-merge(temp3[,c("CommunityPersonID","zipcode","familyid")],addr[,c("familyid","latitude","longitude")],
           by=c("familyid"),all.x=T)

### Fill in missing by zipcode

bylat<-tapply(pll[,"latitude"],pll$zipcode,mean,na.rm=T)
bylon<-tapply(pll[,"longitude"],pll$zipcode,mean,na.rm=T)
latdf<-data.frame(zipcode=names(bylat),latitude_na=as.numeric(bylat))
londf<-data.frame(zipcode=names(bylon),longitude_na=as.numeric(bylon))
byzipll<-merge(latdf,londf,c("zipcode"),all=T)

pll_na<-merge(pll,byzipll,c("zipcode"),all.x=T)
pll_na$latitude<-ifelse(is.na(pll_na$latitude) & !is.na(pll_na$latitude_na),pll_na$latitude_na,pll_na$latitude)
pll_na$longitude<-ifelse(is.na(pll_na$longitude) & !is.na(pll_na$longitude_na),pll_na$longitude_na,pll_na$longitude)

sum(is.na(pll_na$latitude))

save(pll_na,file="pll_na.RData")

library(fields)

dist_out<-rdist.earth(pll_na[,c("longitude","latitude")],hosploc1)
mindist<-do.call(pmin,as.data.frame(dist_out))
hist(mindist)
MinDist<-data.frame(CommunityPersonID=pll_na$CommunityPersonID,disttoh=mindist)

temp3_1<-merge(temp3,MinDist,c("CommunityPersonID"),all.x=T)


### Remove women having mammogram in last 2 years? 
### Add women not having a repeat visit in last two years not incl already?
load("pts.RData")
ptemp<-pts[pts$TenantID==127,]
procedure<-grep("MAMMOG",ptemp$ServiceDesc)

table(ptemp$ServiceDesc[procedure],ptemp$ServiceType[procedure])

summary(ptemp$admitdate[procedure])

exclude<-ptemp[procedure,c("CommunityPersonID","admitdate","CommunityPersonID_orig")]
exclude1<-exclude[order(exclude$CommunityPersonID,exclude$admitdate,decreasing=T),]
exclude2<-exclude1[!duplicated(exclude1$CommunityPersonID),] ## Most recent visit

## Force exclude anyone having gotten mammogram in last 20 months
exclude_f<-exclude2[exclude2$admitdate > (Sys.Date()-600),"CommunityPersonID"]

## Force include anyone having not gotten one since end of 2011
include_f<-exclude2[exclude2$admitdate <= as.Date("12/31/2011","%m/%d/%Y"),"CommunityPersonID"]

### sort/dedup and adjust based on force include, exclude

temp3_1$disttoh<-ifelse(temp3_1$CommunityPersonID %in% exclude_f,100,temp3_1$disttoh)
temp3_1$disttoh<-ifelse(temp3_1$CommunityPersonID %in% include_f,0,temp3_1$disttoh)

temp3_2<-temp3_1[order(temp3_1$AddressID,temp3_1$disttoh),]
temp3_3<-temp3_2[!duplicated(temp3_1$AddressID),]
temp3_3_1<-temp3_3[temp3_3$zipcode %in% zips$Zipcode,]  ### Only take zips in service area (only 71 force incl from outside service area)
temp3_4<-temp3_3_1[order(temp3_3_1$disttoh),]
summary(temp3_4[1:30987,"disttoh"])

tempout<-temp3_4[1:30987,c("CommunityPersonID","AddressID")]
tempout$mailflag <-1
tempout$mailflag[sample.int(28170*1.1,.1*28170)]<-0

### Check 95% quantiles for this given response rate of p
p<-.1

ctrln<-.1*28170
total<-28170*1.1

p
##Minus
p-(qhyper(.025,total*p,total*(1-p),ctrln)/ctrln)
##Plus
(qhyper(.975,total*p,total*(1-p),ctrln)/ctrln)-p



### Now pull addresses for Deb and output
dbname<-"IRMDm_ClaMedCen78"
sql<-odbcConnect("IrmSqls")

system.time(
  outtodeb<-as.data.frame(sqlQuery(sql,
                                   paste0("SELECT 
                                          cp.CommunityPersonID, 
                                          cp.CommunityHouseholdID,
                                          a.AddressID,
                                          cp.firstname, 
                                          cp.middleinitial, 
                                          cp.lastname,a.housenumber,
                                          a.StreetPreDirectional,
                                          a.StreetName,
                                          a.StreetSuffix,
                                          a.StreetPostDirectional,
                                          a.unitnumber,
                                          a.city,
                                          a.state,
                                          a.zipcode, 
                                          g.gendercode, 
                                          findincome, 
                                          wealthfinder,
                                          CASE WHEN FLOOR(DATEDIFF(day, cp.DateOfBirth, GETDATE()) / 365.25) >= 89 THEN '90+'
                                          else cast(FLOOR(DATEDIFF(day, cp.DateOfBirth, GETDATE()) / 365.25) as varchar(3))
                                          END  AS ExactAge, 
                                          hohagecode
                                          from ", dbname, ".dbo.communityperson cp 
                                          left join ", dbname, ".dbo.communityhousehold ch on cp.communityhouseholdid = ch.communityhouseholdid
                                          left join ", dbname, ".dbo.householddemographic hd on ch.communityhouseholdid = hd.communityhouseholdid
                                          left join ", dbname, ".dbo.address a on ch.addressid = a.addressid
                                          left join ", dbname, ".dbo.gender g on cp.genderid = g.genderid")
                                   ,errors=T,stringsAsFactors = F))
)


finalfinalm<-merge(outtodeb,tempout[,c("CommunityPersonID","mailflag")],
               by=c("CommunityPersonID"), all.y=TRUE)

table(finalfinalm$mailflag,dnn=c("Mailflag"),useNA="ifany")

save(finalfinalm,file="final_output_mammogram.RData")

write.csv(finalfinalm,file="Claxton_Mammogram_09_12_2014.csv",quote=F,row.names=F)

